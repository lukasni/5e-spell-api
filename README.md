5e Spell API
============

HTTP API to query a D&D 5e spell database, intended for a slack bot.

Includes a web scraper to scrape spell descriptions from dnd-spells.com


Make sure lxml dependencies are installed

    sudo apt-get install python-dev libxml2-dev libxslt1-dev zlib1g-dev python3-dev


Using virtualenv and virtualenvwrapper, create new environment

    mkproject --python=`which python3` spell-api


Install dependencies using pip

    pip install -r requirements.txt



Wizards of the Coast, Dungeons & Dragons, and their logos are trademarks of Wizards of the Coast LLC in the United States and other countries. © 2016 Wizards. All Rights Reserved.
