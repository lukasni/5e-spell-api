from flask import Flask, jsonify
import models

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False

db = models.db

@app.route('/dnd/v1.0/json/spells/<spell_name>', methods=['GET'])
def get_spell_json(spell_name):
    spell = models.Spell.select().where(models.Spell.name.startswith(spell_name)).dicts().get()
    if len(spell) == 0:
        print("not found")
        abort(404)
    return jsonify({'spell': spell}), 200, {'Content-Type': 'text/json; charset=utf-8'}

@app.route('/dnd/v1.0/md/spells/<spell_name>', methods=['GET'])
def get_spell_markdown(spell_name):
    spell = models.Spell.select().where(models.Spell.name.startswith(spell_name))
    if len(spell) == 0:
        result = { 'response_type': 'ephemeral', 'text': 'Spell not found' }
        return jsonify(result), 404, {'Content-Type': 'text/json; charset=utf-8'}

    spell = spell.dicts().get()

    markdown='''*{name}*
_{school}_
*Level*: {level}
*Casting Time*
{casting_time}

{description}
'''.format(**spell)

    if spell['higher_levels'] != '':
        markdown += '''*At higher levels*
{higher_levels}
'''.format(**spell)

    markdown += '_{long_source}_'.format(**spell)

    result = {
            'response_type': 'in_channel',
            'attachments': [
                    {
                            'text': markdown
                    }
            ]
    }

    return jsonify(result), 200, {'Content-Type': 'text/json; charset=utf-8'}


if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")

