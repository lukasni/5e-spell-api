from peewee import *
import os


db = SqliteDatabase('{0}/spells.db'.format(os.path.dirname(__file__)))

class Spell(Model):
    name = CharField()
    level = IntegerField()
    school = CharField()
    casting_time = CharField()
    ritual = BooleanField()
    concentration = BooleanField()
    source = CharField()
    long_source = CharField(null=True)
    description = TextField(null=True)
    higher_levels = TextField(null=True)

    class Meta:
        database = db


