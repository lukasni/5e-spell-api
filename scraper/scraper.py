from lxml import html
import requests

def url_format_name(name):
    substitutions = {
            ' ': '-',
            '(': '',
            ')': '',
            '/': ''
        }

    name = name.lower()

    for match, sub in substitutions.items():
        name = name.replace(match, sub)

    return name


def get_url(name):
    base_url = "https://www.dnd-spells.com/spell/"

    return base_url + url_format_name(name)


def get_html(name):
    page = requests.get(get_url(name))
    tree = html.fromstring(page.content)

    return tree


def get_description(html):
    desc = "{0}".format(html.xpath('//div[@class="page-content"]//p[3]')[0].text_content())

    return desc.strip()


def get_higherlevels(html):
    if not html.xpath('//h4[@class="classic-title"]'):
        return ""

    desc = "{0}".format(html.xpath('//div[@class="page-content"]//p[4]')[0].text_content())

    return desc.strip()


def get_source(html):
    desc = ""

    if html.xpath('//h4[@class="classic-title"]'):
        desc = "{0}".format(html.xpath('//div[@class="page-content"]//p[5]')[0].text_content())
    else:
        desc = "{0}".format(html.xpath('//div[@class="page-content"]//p[4]')[0].text_content())

    return desc.strip()


def get_all_info(spell):

    html = get_html(spell)

    return {
            "name": spell,
            "description": get_description(html),
            "higherlevels": get_higherlevels(html),
            "source": get_source(html)
            }


def print_all_info(spell):

    html = get_html(spell)

    print(spell)
    print(get_description(html))
    print(get_higherlevels(html))
    print(get_source(html))

